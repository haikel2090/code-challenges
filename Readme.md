![CodeSignal](https://i.ibb.co/Fx6WQXb/code.jpg)![LeetCode](https://i.ibb.co/HzvmgKy/leetcode.png)![CodeWars](https://i.ibb.co/qNFNVhN/codewars.jpg)

### From September 2019 👽
- **Difficulty hard**  
Trapping Rain Water : [link](https://github.com/haikelfazzani/code-challenges/blob/master/TrappingRainWater.md)

- **Difficulty easy**  
Reverse No Numbers : [link](https://github.com/haikelfazzani/code-challenges/blob/master/reverseNoNumbers.md)  
Most Occuring Char : [link](https://github.com/haikelfazzani/code-challenges/blob/master/mostOccuringChar.md)

### 2017-2018-2019 📋 😜
- [CodeSignal challenges solutions](https://github.com/haikelfazzani/CodeFights)
- [LeetCode challenges solutions](https://github.com/haikelfazzani/LeetCode)


### My Accounts
- [CodeSignal](https://app.codesignal.com/profile/haikel)  
- [LeetCode](https://leetcode.com/haikel)
- [CodeWars](https://www.codewars.com/users/Haikel)

