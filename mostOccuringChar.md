### Description
Given a string s, return the character that appears the most number of times in that string.  

If multiple characters appear most number of times, return the character that appears first in the string.  

Example  
For s = "banana", the output should be mostOccuringChar(s) = "a"  
For s = "baba", the output should be mostOccuringChar(s) = "b"

### Solution 1
```js
function mostOccuringChar(s) {
  let result= '', max = 0;
  [...s].map(c => {
    let l = s.split(c).length
    if(l > max) {
        max = l
        result = c        
     }      
  });
  return result;
}
```
### Solution 2
```js
function mostOccuringChar (s) {
    let result = '' , max = 0
    let objLetterCount = [...s].reduce((a,c) => (a[c] = -~a[c], a), {});
    
    Object.keys(objLetterCount).map(c=> {
        if(objLetterCount[c] > max) {                        
            max = objLetterCount[c]
            result = c
        }
    });
    return result;
}
```