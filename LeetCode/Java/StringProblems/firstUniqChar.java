/*
Given a string, find the first non-repeating character in it and return it's index. If it doesn't exist, return -1.

s = "leetcode"  return 0.

s = "loveleetcode", return 2.
*/
public int firstUniqChar(String s) {
  int[] table = new int[26];
  for(int i = 0 ; i < s.length() ;i++){
    table[s.charAt(i) - 'a']++;
  }
  for(int i = 0 ; i < s.length() ;i++){
    if(table[s.charAt(i) - 'a'] == 1){
      return i;
    }
  }
  return -1;
}

Another Solution :

public int firstUniqChar(String s) {
  for(int i = 0 ; i < s.length() ;i++){
    if(s.indexOf(s.charAt(i)) == s.lastIndexOf(s.charAt(i))
      return i;
  }
  return -1;
}
