class Solution {
    public boolean wordPattern(String pattern, String str) {      
        String[] strs = str.split(" ");     
        if(strs.length!=pattern.length()) 
            return false;        
        HashMap<String,Integer> map = new HashMap<String,Integer>();      
        for(Integer i = 0; i < pattern.length() ; i++){
            char c = pattern.charAt(i);
            if(map.put(c + " ",i) != map.put(strs[i],i)){
                return false;
            }
        }
        return true;
    }
}

/*
Given a pattern and a string str, find if str follows the same pattern.

Here follow means a full match, such that there is a bijection between a letter in pattern and a non-empty word in str.

Examples:
pattern = "abba", str = "dog cat cat dog" should return true.
pattern = "abba", str = "dog cat cat fish" should return false.
pattern = "aaaa", str = "dog cat cat dog" should return false.
pattern = "abba", str = "dog dog dog dog" should return false.
*/
