digitDifferenceSort = a => {    
    let x = []
    
    a.map((i, n) => {
        
        a = (i+``).split(``).sort()
        x.push({ i, n, d: +[...a].pop()-a[0] })        
    })        
    
    return x.sort((j,k)=> j.d - k.d || k.n-j.n).map(o=> o.i)
}

/* Another short solution */
digitDifferenceSort = a => 
    a.map((i, n) => 
    ({ i, n, d: (
        i = [...i+``].sort(), [...i].pop() - i[0] ) 
     })
    )
    .sort((j,k) => j.d - k.d || k.n-j.n).map(o => o.i)


/*
    Note :  [...a].pop() => will return last element of array
            +[...a].pop()-a[0] => will cast to number instead using Number() or parseInt()
*/ 

/*
https://app.codesignal.com/challenge/qd43krQ8ckyP83eKF

Given an array of integers, sort its elements by the difference of 
their largest and smallest digits. In the case of a tie, that with the larger index in the array 
should come first.

Example

For a = [152, 23, 7, 887, 243], the output should be
digitDifferenceSort(a) = [7, 887, 23, 243, 152].

Here are the differences of all the numbers:

152: difference = 5 - 1 = 4;
23: difference = 3 - 2 = 1;
7: difference = 7 - 7 = 0;
887: difference = 8 - 7 = 1;
243: difference = 4 - 2 = 2.
23 and 887 have the same difference, but 887 goes after 23 in a, so in the sorted array it comes first.

Solution : 
*/