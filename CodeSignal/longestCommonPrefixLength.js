j = 0
longestCommonPrefixLength = s => {
    if((l = s.length) < 1) return 0
    s.sort() 
    while(j < s[0].length & s[0][j] == s[l -1][j]) j++    
    return j
}


/*
    Given an array of strings, return the length of the longest common prefix amongst the strings.

    Example

    For input: ["abc", "ab", "a"]
    output should be 1

    For input: ["code", "codesignal", "codesign"]
    output should be 4

    For input: ["python", "java", "scala", "javascript", "php"]
    output should be 0
*/