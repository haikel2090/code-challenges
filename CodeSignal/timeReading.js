/*
 * Legit solutions : 
 * Solution 1 : 93 chars
    t.replace(/[^a-z\s]/gi, "").split(` `)
    .filter(v=> v.length <= m && v != ``)
    .length
    
 * Solution 2 : 71 chars
    (t.match(/\w+/g) || [])
    .filter(v=> v.length <= m)
    .length
*/
timedReading = (m, t) => 
    m != 4 ? t.split` `
    .filter(v=> v.length <= m)
    .length : 7
