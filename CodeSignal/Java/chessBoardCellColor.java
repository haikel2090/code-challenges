boolean chessBoardCellColor(String cell1, String cell2) {
  int sumA = cell2.charAt(1) - '1' + cell1.charAt(1) - '1';
  int sumC = cell2.charAt(0) - '1' + cell1.charAt(0) - '1';
  return sumA % 2 == sumC % 2;
}

boolean chessBoardCellColor(String cell1, String cell2) {
  int sumA = cell1.charAt(0) - 'A' + cell1.charAt(1) - '1';
  int sumC = cell2.charAt(0) - 'A' + cell2.charAt(1) - '1';
  return sumA % 2 == sumC % 2;
}

/*
Given two cells on the standard chess board, determine whether they have the same color or not.

For cell1 = "A1" and cell2 = "C3", the output should be
chessBoardCellColor(cell1, cell2) = true.

For cell1 = "A1" and cell2 = "H3", the output should be
chessBoardCellColor(cell1, cell2) = false.
*/

https://codefights.com/tournaments/Mmjzeu3qxgQ6rZ7Ab/E
