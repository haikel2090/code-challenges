int eulersTotientFunction(int n) {
    int cnt = 0;
    for (int i = 1; i <= n; ++i) {
        if (GCD(n, i) == 1)
            cnt++;
    }
    return cnt;
}

public int GCD(int a, int b) {
   if (b==0) return a;
   return GCD(b,a%b);
}
or // public int GCD(int a, int b) { return b==0 ? a : GCD(b, a%b); }
/*
Given an integer n, find the value of phi(n), where phi is Euler's totient function.

Example

For n = 5, the output should be
eulersTotientFunction(n) = 4.
*/
