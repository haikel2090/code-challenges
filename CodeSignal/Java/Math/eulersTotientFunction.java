int eulersTotientFunction(int n) {
    int p = 2;
    int result = n;
    while ((int)Math.pow(p * p <= n) {
      if (n % p == 0) {
        while (n % p == 0) n /= p;
        result -= result / p;
      }
      p++;
    }
    if (n > 1) result -= result / n;
    return result;
}

/*
Given an integer n, find the value of phi(n), where phi is Euler's totient function.
Note : Euler’s totient or phi function is an arithmetic function that counts the positive integers less than 
or equal to n that are relatively prime to n.
Example
For n = 5, the output should be
eulersTotientFunction(n) = 4.
*/
