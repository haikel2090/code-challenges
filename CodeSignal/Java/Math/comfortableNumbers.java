int check(int x){
    return x > 0 ? check(x / 10) + x % 10 : 0;
}
int comfortableNumbers(int l, int r) {
  int result = 0;
    for (int i = l; i <= r; i++){
        for (int j = i + 1; j <= r; j++)
            if (Math.abs(i - j) <= Math.min(check(i), check(j)))
                result++;
    }
  return result;
}

https://codefights.com/tournaments/aZHnSjt8jeYLPGu89/C
