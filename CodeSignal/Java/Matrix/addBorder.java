String[] addBorder(String[] picture) {
    String [] arr= new String [picture.length+2];
    String s = "";
    for(int i = 0; i < picture[0].length()+2 ; i++){
        s = s + "*";
    }
    
    arr[0] = s; arr[arr.length-1] = s;  
    
    for(int j = 0; j < picture.length ; j++){
        arr[j+1]= "*" + picture[j] + "*";
    }
    return arr;
}

/*
Given a rectangular matrix of characters, add a border of asterisks(*) to it.

picture = ["abc",
           "ded"]
the output should be

addBorder(picture) = ["*****",
                      "*abc*",
                      "*ded*",
                      "*****"]
*/
