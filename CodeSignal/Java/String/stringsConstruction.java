int stringsConstruction(String a, String b) {
    int[] c = new int[26];
      for (int i=0; i < b.length() ; i++) {
              c[b.charAt(i) - 'a']++;
      }
     int s = 0;
     while (true) {
        for (int i=0; i < a.length() ; i++) {
            c[a.charAt(i) - 'a']--;
            if (c[a.charAt(i) - 'a'] < 0) {
                return s;
            }
        }
        s++;
     } 
}

/*How many strings equal to a can be constructed using letters from the string b? 
Each letter can be used only once and in one string only.

For a = "abc" and b = "abccba", the output should be
stringsConstruction(a, b) = 2.

We can construct 2 strings a with letters from b.
*/
