String longestDigitsPrefix(String s) {
    String res = "";
    for(int i = 0 ; i < s.length() ; i++){
        if(Character.isDigit(s.charAt(i)))
            res += (char)s.charAt(i);
        else break;
    }
    return res;
}

/*
Given a string, output its longest prefix which contains only digits.
For inputString="123aa1", the output should be
longestDigitsPrefix(inputString) = "123"
*/
