boolean alphabetSubsequence(String s) {
for(int i=1 ; i< s.length() ; ++i)
        if(s.charAt(i) <= s.charAt(i-1))
            return false;
    
    return true;
}

/*
Check whether the given string is a subsequence of the plaintext alphabet.

Note : A subsequence is a sequence that can be derived from another sequence by deleting some elements (possibly, none) 
without changing the order of the remaining elements.
Note : The plaintext alphabet is a string "abcdef...xyz".

For s = "effg" or s = "cdce", the output should be
alphabetSubsequence(s) = false;
For s = "ace" or s = "bxz", the output should be
alphabetSubsequence(s) = true.
*/
