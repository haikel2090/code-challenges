String reflectString(String inputString) {
    String str ="";
    for(int i=0; i<inputString.length() ; i++){
        str +=""+(char)(219-inputString.charAt(i));
         
    }
    return str;
}

Another Solution :

String reflectString(String inputString) {
    String s ="";
    for(char c : inputString.toCharArray()){
        int i = 'z' - c;
        s +=(char)( i + 'a');
    }
    return s;
}

/*
Define an alphabet reflection as follows: a turns into z, b turns into y, 
c turns into x, ..., n turns into m, m turns into n, ..., z turns into a.

Define a string reflection as the result of applying the alphabet reflection to each of its characters.

Reflect the given string.

For inputString = "name", the output should be
reflectString(inputString) = "mznv".
*/
