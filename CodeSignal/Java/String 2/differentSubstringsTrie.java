int differentSubstringsTrie(String inputString) {
    HashSet<String> set = new HashSet();
    for(int i=0; i<inputString.length(); i++) {
        String r = "";
        for(int j=i; j<inputString.length(); j++) {
            r += inputString.charAt(j);
            set.add(r);
        }
    }
    return set.size();
}

Given a string, find the number of different non-empty substrings in it.
Note : A substring of a string S is another string S' that occurs in S. For example, 
"Fights" is a substring of "CodeFights", but "CoFi" isn't.

For inputString = "abac", the output should be
differentSubstringsTrie(inputString) = 9.
They are:

"a", "b", "c",
"ab", ac", "ba",
"aba", "bac",
"abac"
