/*iqTest = a =>    	
	        a.length < 1 ? 0 :
            (a = a.split(" "))
            .reduce((k,i) => k += i % 2 < 1 ,0) ? 
			a.findIndex(v => v % 2 > 0)+1
		  : a.findIndex(v => v % 2 < 1)+1
*/

iqTest = a =>    	
	a.length ? 
      (a = a.split(` `))
      .filter(i => i % 2).length > 1 ? 
        a.findIndex(v => v % 2 < 1)+1      
      : a.findIndex(v => v % 2)+1
    : 0


/*
Examples :

iqTest("2 4 7 8 10") => 3 // Third number is odd, while the rest of the numbers are even

iqTest("1 2 1 1") => 2 // Second number is even, while the rest of the numbers are odd
*/