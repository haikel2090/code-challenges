r = 0
sortByFreq = s => {

 r = (k=[...s]).reduce((a,c) => ( a[c] ? a[c]++ : a[c] = 1, a), {})
 
 return k.sort((i,j) => r[j] - r[i] || i.localeCompare(j))
        .join(``)

}


/**/

r = 0
sortByFreq = s => {

 r = (k=[...s]).reduce((a,c) => ( a[c] = -~a[c], a), {})
 
 return k.sort((i,j) => r[j] - r[i] || i.localeCompare(j)).join(``)
}

/*
    Given a string s, sort it in descending order based on the frequency of characters.
    If two characters a and b have the same character count, the smallest should come first.

    s: "tree"
    Expected Output:
    "eert"

    s: "dcba"
    Expected Output:
    "abcd"
*/