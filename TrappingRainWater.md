### Description
Given n non-negative integers representing an elevation map where the width of each bar is 1, compute how much water it is able to trap after raining.

![img](https://image.noelshack.com/fichiers/2019/37/4/1568249797-rainwatertrap.png)

The above elevation map is represented by array [0,1,0,2,1,0,1,3,2,1,2,1]. In this case, 6 units of rain water (blue section) are being trapped.

Example:
```
trap([0,1,0,2,1,0,1,3,2,1,2,1]) = 6
```

### Solution
```js
function trap (height) {  
  let i = 0, j = height.length-1;
  let leftMax = 0, rightMax = 0 , result = 0;
      
  while(i <= j){
      
      height[i] > left ? leftMax = height[i] : v += leftMax - height[i]
      height[j] > rightMax ? rightMax = height[j] : v += rightMax - height[j]
      
      result > leftMax ? i++ : j--;
  }    
  return result;
}
```

**Another test case**
```
For : [1, 0, 1] -> Expected Output: 1
For : [1, 0, 1, 0, 2, 3] -> Expected Output: 2
For : [10, 6, 5, 0, 7, 2, 9, 11, 25, 3] -> Expected Output: 31
```